package com.hw.db.dao;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

public class ForumDaoTest {

    JdbcTemplate jdbcMock = Mockito.mock(JdbcTemplate.class);
    ForumDAO forumDao;

    private final String USER_LIST_TEST_1 = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;";

    private final String USER_LIST_TEST_2 = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname LIMIT ?;";

    private final String USER_LIST_TEST_3 = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname LIMIT ?;";

    private final String USER_LIST_TEST_4 = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;";

    private final String USER_LIST_TEST_5 = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc;";

    private final String USER_LIST_TEST_6 = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc LIMIT ?;";

    private final String USER_LIST_TEST_7 = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;";

    private final String USER_LIST_TEST_8 = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc;";

    @BeforeEach
    void init() {
        this.forumDao = new ForumDAO(jdbcMock);
    }

    @Test
    void furumUserListTest1() {
        ForumDAO.UserList("any", null, null, false);
        Mockito.verify(jdbcMock).query(
                Mockito.eq(USER_LIST_TEST_1),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void furumUserListTest2() {
        ForumDAO.UserList("any", 10, null, false);
        Mockito.verify(jdbcMock).query(
                Mockito.eq(USER_LIST_TEST_2),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void furumUserListTest3() {
        ForumDAO.UserList("any", 10, "10.10.2010", false);
        Mockito.verify(jdbcMock).query(
                Mockito.eq(USER_LIST_TEST_3),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void furumUserListTest4() {
        ForumDAO.UserList("any", null, "10.10.2010", false);
        Mockito.verify(jdbcMock).query(
                Mockito.eq(USER_LIST_TEST_4),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void furumUserListTest5() {
        ForumDAO.UserList("any", null, null, true);
        Mockito.verify(jdbcMock).query(
                Mockito.eq(USER_LIST_TEST_5),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void furumUserListTest6() {
        ForumDAO.UserList("any", 10, null, true);
        Mockito.verify(jdbcMock).query(
                Mockito.eq(USER_LIST_TEST_6),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void furumUserListTest7() {
        ForumDAO.UserList("any", 10, "10.10.2010", true);
        Mockito.verify(jdbcMock).query(
                Mockito.eq(USER_LIST_TEST_7),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void furumUserListTest8() {
        ForumDAO.UserList("any", null, "10.10.2010", true);
        Mockito.verify(jdbcMock).query(
                Mockito.eq(USER_LIST_TEST_8),
                Mockito.any(Object[].class),
                Mockito.any(UserDAO.UserMapper.class));
    }
}
