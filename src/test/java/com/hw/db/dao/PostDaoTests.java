package com.hw.db.dao;

import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Random;

public class PostDaoTests {

    JdbcTemplate jdbcMock = Mockito.mock(JdbcTemplate.class);

    PostDAO postDAO = new PostDAO(jdbcMock);

    Random random = new Random();

    Post existingPost;

    private final String TEST_QUERY_1 = "UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;";

    private final String TEST_QUERY_2 = "UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;";

    private final String TEST_QUERY_3 = "UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;";

    private final String TEST_QUERY_4 = "UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;";

    private final String TEST_QUERY_5 = "UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;";

    private final String TEST_QUERY_6 = "UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;";

    private final String TEST_QUERY_7 = "UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;";

    @BeforeEach
    void setUp() {
        this.existingPost = getPost(
                RandomString.make(10),
                RandomString.make(10),
                Timestamp.valueOf(LocalDateTime.now().plusHours(random.nextInt(10))),
                random.nextInt(10));

        Mockito.when(jdbcMock.queryForObject(
                        Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                        Mockito.any(PostDAO.PostMapper.class),
                        Mockito.any()))
                .thenReturn(existingPost);
    }

    @Test
    void postDaoTest1() {
        Post post2 = getPost(
                RandomString.make(10),
                existingPost.getMessage(),
                existingPost.getCreated(),
                existingPost.getId());

        PostDAO.setPost(existingPost.getId(), post2);

        Mockito.verify(jdbcMock).update(
                Mockito.eq(TEST_QUERY_1),
                Mockito.anyString(),
                Mockito.anyInt()
        );
    }

    @Test
    void postDaoTest2() {
        Post post2 = getPost(
                existingPost.getAuthor(),
                RandomString.make(10),
                existingPost.getCreated(),
                existingPost.getId());

        PostDAO.setPost(existingPost.getId(), post2);

        Mockito.verify(jdbcMock).update(
                Mockito.eq(TEST_QUERY_2),
                Mockito.anyString(),
                Mockito.anyInt()
        );
    }

    @Test
    void postDaoTest3() {
        Post post2 = getPost(
                existingPost.getAuthor(),
                existingPost.getMessage(),
                Timestamp.valueOf(LocalDateTime.now().minusHours(random.nextInt(10))),
                existingPost.getId());

        PostDAO.setPost(existingPost.getId(), post2);

        Mockito.verify(jdbcMock).update(
                Mockito.eq(TEST_QUERY_3),
                Mockito.any(Timestamp.class),
                Mockito.anyInt()
        );
    }

    @Test
    void postDaoTest4() {
        Post post2 = getPost(
                RandomString.make(10),
                RandomString.make(10),
                existingPost.getCreated(),
                existingPost.getId());

        PostDAO.setPost(existingPost.getId(), post2);

        Mockito.verify(jdbcMock).update(
                Mockito.eq(TEST_QUERY_4),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.anyInt()
        );
    }

    @Test
    void postDaoTest5() {
        Post post2 = getPost(
                RandomString.make(10),
                RandomString.make(10),
                Timestamp.valueOf(LocalDateTime.now().minusHours(random.nextInt(10))),
                existingPost.getId());

        PostDAO.setPost(existingPost.getId(), post2);

        Mockito.verify(jdbcMock).update(
                Mockito.eq(TEST_QUERY_5),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.any(Timestamp.class),
                Mockito.anyInt()
        );
    }

    @Test
    void postDaoTest6() {
        Post post2 = getPost(
                existingPost.getAuthor(),
                RandomString.make(10),
                Timestamp.valueOf(LocalDateTime.now().minusHours(random.nextInt(10))),
                existingPost.getId());

        PostDAO.setPost(existingPost.getId(), post2);

        Mockito.verify(jdbcMock).update(
                Mockito.eq(TEST_QUERY_6),
                Mockito.anyString(),
                Mockito.any(Timestamp.class),
                Mockito.anyInt()
        );
    }

    @Test
    void postDaoTest7() {
        Post post2 = getPost(
                RandomString.make(10),
                existingPost.getMessage(),
                Timestamp.valueOf(LocalDateTime.now().minusHours(random.nextInt(10))),
                existingPost.getId());

        PostDAO.setPost(existingPost.getId(), post2);

        Mockito.verify(jdbcMock).update(
                Mockito.eq(TEST_QUERY_7),
                Mockito.anyString(),
                Mockito.any(Timestamp.class),
                Mockito.anyInt()
        );
    }

    @Test
    void postDaoTest8() {
        Post post2 = getPost(
                existingPost.getAuthor(),
                existingPost.getMessage(),
                existingPost.getCreated(),
                existingPost.getId());

        PostDAO.setPost(existingPost.getId(), post2);

        Mockito.verify(jdbcMock, Mockito.never()).update(Mockito.anyString(), Mockito.any(Object[].class));
    }

    private Post getPost(String author,
                         String message,
                         Timestamp created,
                         Integer id) {
        Post post = new Post();
        post.setAuthor(author);
        post.setMessage(message);
        post.setCreated(created);
        post.setId(id);
        return post;
    }
}
