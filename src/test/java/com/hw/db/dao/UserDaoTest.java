package com.hw.db.dao;

import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

public class UserDaoTest {

    JdbcTemplate jdbcMock = Mockito.mock(JdbcTemplate.class);

    UserDAO userDAO  = new UserDAO(jdbcMock);

    private final String TEST_QUERY_1 = "UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;";

    private final String TEST_QUERY_2 = "UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;";

    private final String TEST_QUERY_3 = "UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;";

    @Test
    void testUserDaoUpdate1() {
        User user = getUser();
        user.setFullname(RandomString.make(10));
        UserDAO.Change(user);

        Mockito.verify(jdbcMock).update(
                Mockito.eq(TEST_QUERY_1),
                Mockito.anyString(),
                Mockito.anyString()
        );
    }

    @Test
    void testUserDaoUpdate2() {
        User user = getUser();
        user.setEmail(RandomString.make(10));
        UserDAO.Change(user);

        Mockito.verify(jdbcMock).update(
                Mockito.eq(TEST_QUERY_2),
                Mockito.anyString(),
                Mockito.anyString()
        );
    }

    @Test
    void testUserDaoUpdate3() {
        User user = getUser();
        user.setAbout(RandomString.make(10));
        UserDAO.Change(user);

        Mockito.verify(jdbcMock).update(
                Mockito.eq(TEST_QUERY_3),
                Mockito.anyString(),
                Mockito.anyString()
        );
    }

    private User getUser() {
        User user = new User();
        user.setNickname(RandomString.make(10));
        return user;
    }
}
