package com.hw.db.dao;

import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

public class ThreadDaoTest {

    JdbcTemplate jdbcMock = Mockito.mock(JdbcTemplate.class);

    ThreadDAO threadDAO = new ThreadDAO(jdbcMock);

    private final String TEST_QUERY_1 = "SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;";

    private final String TEST_QUERY_2 = "SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;";

    @Test
    void testTestDao1() {
        ThreadDAO.treeSort(100, 100, 20, false);

        Mockito.verify(jdbcMock).query(
                Mockito.eq(TEST_QUERY_1),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.any());
    }

    @Test
    void testTestDao2() {
        ThreadDAO.treeSort(100, 100, 20, true);

        Mockito.verify(jdbcMock).query(
                Mockito.eq(TEST_QUERY_2),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.any());
    }
}
